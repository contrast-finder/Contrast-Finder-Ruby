# Contrast-Finder-Ruby

This is a (re)implementation of [Contrast-Finder](https://contrast-finder.org/) in Ruby.

Objectives:

- discover Ruby and its libraries
- implement new algorithms to compute color contrast (and evaluate whether it is easier than in Java, either for me and for potential contributors)

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'Contrast-Finder-Ruby'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install Contrast-Finder-Ruby

## Usage

TODO: Write usage instructions here

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome at gitlab.com/asqatasun/Contrast-Finder-Ruby](https://gitlab.com/asqatasun/Contrast-Finder-Ruby). This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

This software is licensed under aGPL

## Contact

Feel free to get in touch with us at `hello AT contrast-finder DOT org`

